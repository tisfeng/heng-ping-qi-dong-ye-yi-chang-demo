//
//  ViewController.m
//  横屏启动页异常
//
//  Created by isfeng on 2019/1/13.
//  Copyright © 2019 isfeng. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [_button setTitle:NSLocalizedString(@"buttonTitle",nil) forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(changeSystemLanguage:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)changeSystemLanguage:(UIButton *)button {
    NSString *appleLanguages = @"AppleLanguages";
    NSArray *languagesArray = [[NSUserDefaults standardUserDefaults] valueForKey:appleLanguages];
    NSString *currentlanguage = languagesArray.firstObject;
    if ([currentlanguage containsString:@"en"]) { //en-US,en
        currentlanguage = @"zh-Hans";
    } else {
        currentlanguage = @"en";
    }
    languagesArray = @[currentlanguage];
    [[NSUserDefaults standardUserDefaults] setObject:languagesArray forKey:appleLanguages];
}
- (void)aboutSystemLanguage {
    // 获取当前系统语言
    NSArray *langArr1 = [[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
    NSString *language1 = langArr1.firstObject;
    NSLog(@"模拟器语言切换之前：%@",language1);
    
    // 切换语言
    NSArray *lans = @[@"en"];
    [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
    
    // 切换语言后
    NSArray *langArr2 = [[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
    NSString *language2 = langArr2.firstObject;
    NSLog(@"模拟器语言切换之后：%@",language2);
}


- (void)viewWillAppear:(BOOL)animated {

    /**
     问题：App 启动后横屏，然后杀死 App 再重启，启动页会显示异常（90°旋转）。
     
     问题复现条件：
     1. 手机设备要是 iPhone X （或以上，待定）；
     2. App 中使用了屏幕旋转横屏功能；
     3. App 杀死后再手动启动，并且以非竖屏形式启动。
     
     探究：只要 App 中使用了 屏幕旋转功能，在某些机型就可能出现启动页异常问题。（注：无论是使用代码方式旋转屏幕，还是手动调整旋转屏幕，都会出现。）
     
     原因：目前初步判断，这是系统的一个 bug，或说特殊 feature。还需多种机型进一步实测。
     
     解决：不使用动态视图方式加载启动页，改为静态图片方式。（需在 iPhone X 及以上机型测试）
     */
    
    // 取注释下面这行代码，就可以复现启动页异常问题。
//    [[UIDevice currentDevice] setValue:@(UIDeviceOrientationLandscapeLeft) forKey:@"orientation"];
}

@end
