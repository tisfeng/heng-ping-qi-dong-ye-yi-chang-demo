//
//  AppDelegate.h
//  系统横屏情况
//
//  Created by isfeng on 2019/1/13.
//  Copyright © 2019 isfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

