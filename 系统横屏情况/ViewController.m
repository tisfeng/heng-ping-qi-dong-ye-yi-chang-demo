//
//  ViewController.m
//  系统横屏情况
//
//  Created by isfeng on 2019/1/13.
//  Copyright © 2019 isfeng. All rights reserved.
//

#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self playMovies];
}

- (void)playMovies {
//    NSURL *URL = [NSURL fileURLWithPath:[NSBundle pathForResource:@"WWDC.mp4" ofType:nil inDirectory:@"/Users/wangxunfeng/Google Drive/Code/TestCode/横屏启动页异常/系统横屏情况"]];

//    URL = [NSURL URLWithString:@"http://XXX.mp4"]; //网络视频

    // 设置资源路径
    NSString *path  = [[NSBundle mainBundle]  pathForResource:@"WWDC" ofType:@"mp4"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVPlayer *avPlayer= [AVPlayer playerWithURL:url];

    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.player = avPlayer;
    playerViewController.videoGravity = AVLayerVideoGravityResizeAspect;
    playerViewController.showsPlaybackControls = YES;
    playerViewController.view.frame = self.view.bounds;
    // 将播放器控制器添加到当前页面控制器中
    [self addChildViewController:playerViewController];
    // view 一定要添加，否则将不显示
    [self.view addSubview:playerViewController.view];
    [playerViewController.player play];
}


@end
