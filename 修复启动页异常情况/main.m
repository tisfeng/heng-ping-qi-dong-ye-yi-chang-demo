//
//  main.m
//  修复启动页异常情况
//
//  Created by isfeng on 2019/1/13.
//  Copyright © 2019 isfeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
